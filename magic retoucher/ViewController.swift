//
//  ViewController.swift
//  magic retoucher
//
//  Created by kittipong intaboot on 11/26/15.
//  Copyright © 2015 kint studio. All rights reserved.
//

import Foundation
import UIKit
import TOCropViewController


class ViewController: UIViewController, UIImagePickerControllerDelegate, UIPopoverControllerDelegate, UINavigationControllerDelegate {
    
    var picker:UIImagePickerController?=UIImagePickerController();
    
    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        picker?.delegate=self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    // Open Gallery button click
    @IBAction func OpenGallery(sender: AnyObject) {
        openGallary()
    }
    
    // Take Photo button click
    @IBAction func TakePhoto(sender: AnyObject) {
        openCamera()
    }
    
    
    func openGallary()
    {
        picker!.allowsEditing = false
        picker!.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        presentViewController(picker!, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)){
            picker!.allowsEditing = false
            picker!.sourceType = UIImagePickerControllerSourceType.Camera
            picker!.cameraCaptureMode = .Photo
            presentViewController(picker!, animated: true, completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .Alert)
            let ok = UIAlertAction(title: "OK", style:.Default, handler: nil)
            alert.addAction(ok)
            presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController,
        didFinishPickingMediaWithInfo info: [String : AnyObject]){
            let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
            
            dismissViewControllerAnimated(true) { () -> Void in
                let cropViewController = TOCropViewController(image: chosenImage)
                cropViewController.delegate = self
                
                self.presentViewController(cropViewController, animated: true, completion: { () -> Void in
                    
                })
                
            }
    }
    
    @IBAction func nextButtonTapped(sender: AnyObject) {
        if(self.imageView.image == nil){
            return;
        }
        self.performSegueWithIdentifier("ShowGrabCut", sender: nil)
    }
    
    private func resizeImage(image: UIImage, size: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        image.drawInRect(CGRectMake(0, 0, size.width, size.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ShowGrabCut" {
            let secondVC: GrabCutController = segue.destinationViewController as! GrabCutController
            secondVC.imageOriginal = self.imageView.image
        }
    }
}


extension ViewController: TOCropViewControllerDelegate {
    func cropViewController(cropViewController: TOCropViewController!, didCropToImage image: UIImage!, withRect cropRect: CGRect, angle: Int) {
        cropViewController.dismissViewControllerAnimated(true) { () -> Void in
            
        }        
        imageView.image = self.resizeImage(image, size: CGSizeMake(500, 500));
    }
}