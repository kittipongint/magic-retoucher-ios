//
//  TouchDrawView.swift
//  magic retoucher
//
//  Created by kittipong intaboot on 12/8/15.
//  Copyright © 2015 kint studio. All rights reserved.
//


enum TouchState : Int {
    case None
    case Rect
    case Plus
    case Minus
}


class TouchDrawView: UIView {
    
    var pts = [CGPoint]();
    var ctr = 0;
    
    var rectangle : CGRect?;
    var plusPath = UIBezierPath();
    var minusPath = UIBezierPath();
    var incrementalImage : UIImage?;
    var currentState = TouchState.None;

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!;
        
        self.pts = [CGPoint](count: 5, repeatedValue: CGPoint(x: 0.0, y: 0.0));
        self.ctr = 0;
        
        self.rectangle = nil;
        self.plusPath = UIBezierPath();
        self.minusPath = UIBezierPath();
        self.incrementalImage = nil;
        self.currentState = TouchState.None;
        
        self.initTouchView();
    }
    
//    convenience init () {
//        self.init(frame:CGRect.zero)
//    }
//    
//    required init(coder aDecoder: NSCoder) {
//        fatalError("This class does not support NSCoding")
//    }
    

//    required init(coder aDecoder: NSCoder) {
//                //fatalError("This class does not support NSCoding")
//        if super.init(coder: aDecoder){
//            self.initTouchView()
//        }
//    }
    
    func initTouchView() {
        self.plusPath = UIBezierPath();
        self.plusPath.lineWidth = 6.0;
        self.plusPath.lineCapStyle = .Round;
        
        self.minusPath = UIBezierPath();
        self.minusPath.lineWidth = 6.0;
        self.minusPath.lineCapStyle = .Round;
        
        self.currentState = TouchState.None;
    }
    
    func touchStarted(p: CGPoint) {
        if currentState == TouchState.Plus || currentState == TouchState.Minus {
            ctr = 0
            pts[0] = p
        }
    }
    
    func touchMoved(p: CGPoint) {
        if currentState == TouchState.Plus || currentState == TouchState.Minus {
            ctr++
            pts[ctr] = p
            if ctr == 4 {
                pts[3] = CGPointMake((pts[2].x + pts[4].x) / 2.0, (pts[2].y + pts[4].y) / 2.0)
                // move the endpoint to the middle of the line joining the second control point of the first Bezier segment and the first control point of the second Bezier segment
                if currentState == TouchState.Plus {
                    plusPath.moveToPoint(pts[0])
                    plusPath.addCurveToPoint(pts[3], controlPoint1: pts[1], controlPoint2: pts[2])
                    // add a cubic Bezier from pt[0] to pt[3], with control points pt[1] and pt[2]
                }
                else {
                    if currentState == TouchState.Minus {
                        minusPath.moveToPoint(pts[0])
                        minusPath.addCurveToPoint(pts[3], controlPoint1: pts[1], controlPoint2: pts[2])
                        // add a cubic Bezier from pt[0] to pt[3], with control points pt[1] and pt[2]
                    }
                }
                self.setNeedsDisplay()
                // replace points and get ready to handle the next segment
                pts[0] = pts[3]
                pts[1] = pts[4]
                ctr = 1
            }
        }
    }
    
    func touchEnded(p: CGPoint) {
        if currentState == TouchState.Plus || currentState == TouchState.Minus {
            
        }
    }
    
    func drawRectangle(rect: CGRect) {
        self.currentState = TouchState.Rect
        self.rectangle = rect
        self.setNeedsDisplay()
    }
    
    func clear() {
        self.currentState = TouchState.None
        plusPath.removeAllPoints()
        minusPath.removeAllPoints()
        self.setNeedsDisplay()
    }
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
        if currentState == TouchState.Rect {
            //Get the CGContext from this view
            let context: CGContextRef = UIGraphicsGetCurrentContext()!
            //Draw a rectangle
            CGContextSetFillColorWithColor(context, UIColor(red: 1.0, green: 0, blue: 0, alpha: 0.4).CGColor)
            //Define a rectangle
            CGContextAddRect(context, rectangle!)
            //Draw it
            CGContextFillPath(context)
        }
        else {
            if currentState == TouchState.Plus || currentState == TouchState.Minus {
                //        [_incrementalImage drawInRect:rect];
                UIColor.greenColor().setStroke()
                plusPath.stroke()
                UIColor.redColor().setStroke()
                minusPath.stroke()
            }
        }
    }
    
    func maskImageWithPainting() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.opaque, 0.0)
        self.layer.renderInContext(UIGraphicsGetCurrentContext()!)
        let img: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img
    }
}