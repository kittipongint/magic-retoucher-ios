//
//  grabcut.hpp
//  magic retoucher
//
//  Created by kittipong intaboot on 12/2/15.
//  Copyright © 2015 kint studio. All rights reserved.
//

#ifndef CVOpenTemplate_Header_h
#define CVOpenTemplate_Header_h

#include <opencv2/opencv.hpp>

cv::Mat stitch (std::vector <cv::Mat> & images);


#endif /* grabcut_hpp */