//
//  CropImageController.swift
//  magic retoucher
//
//  Created by kittipong intaboot on 11/30/15.
//  Copyright © 2015 kint studio. All rights reserved.
//

import Foundation
import UIKit
import JHSpinner

class GrabCutController: UIViewController{
    
    var startPoint = CGPoint();
    var endPoint = CGPoint();
    var touchState: TouchState?;
    var grabRect: CGRect?;
    
    var imageOriginal : UIImage?;
    
    var gcManager : GrabCutManager = GrabCutManager();
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet var touchDrawView: TouchDrawView!
    @IBOutlet weak var buttonReset: UIButton!
    @IBOutlet weak var buttonRect: UIButton!
    @IBOutlet weak var buttonPlus: UIButton!
    @IBOutlet weak var buttonMinus: UIButton!
    @IBOutlet weak var buttonRetouch: UIButton!
    @IBOutlet weak var buttonSave: UIButton!
    
    override func viewDidLoad() {
        imageView.image = imageOriginal;
        
        touchState = .None;
        grabRect = CGRect();
        
        self.imageView.image = imageOriginal;
        self.touchState = .Plus;
        //self.updateStateLabel();
        self.buttonRect.enabled = true;
        self.buttonPlus.enabled = false;
        self.buttonMinus.enabled = false;
        self.buttonRetouch.enabled = false;
        self.touchDrawView.clear();
        self.gcManager.resetManager();
    }
    
    
    func changeImageViewBackground(color: UIColor){
        imageView.backgroundColor = color;
    }
    
    
    
    // MARK: - Tapped event
    
    @IBAction func bgColorButton1Tapped(sender: AnyObject) {
        changeImageViewBackground(UIColor.whiteColor());
    }
    
    @IBAction func bgColorButton2Tapped(sender: AnyObject) {
        changeImageViewBackground(UIColor.grayColor());
    }
    
    @IBAction func bgColorButton3Tapped(sender: AnyObject) {
        changeImageViewBackground(UIColor.blackColor());
    }
    
    @IBAction func bgColorButton4Tapped(sender: AnyObject) {
        changeImageViewBackground(UIColor.blueColor());
    }
    
    @IBAction func bgColorButton5Tapped(sender: AnyObject) {
        changeImageViewBackground(UIColor.cyanColor());
    }
    
    @IBAction func bgColorButton6Tapped(sender: AnyObject) {
        changeImageViewBackground(UIColor.redColor());
    }
    
    
    @IBAction func buttonResetTapped(sender: AnyObject) {
        self.imageView.image = imageOriginal;
        self.touchState = .None;
        //self.updateStateLabel();
        self.buttonRect.enabled = true;
        self.buttonPlus.enabled = false;
        self.buttonMinus.enabled = false;
        self.buttonRetouch.enabled = false;
        self.touchDrawView.clear();
        self.gcManager.resetManager();
    }
    
    @IBAction func buttonRectTapped(sender: AnyObject) {
        NSLog("rect")
        self.touchState = .Rect;
        //self.updateStateLabel();
        self.buttonPlus.enabled = false;
        self.buttonMinus.enabled = false;
        self.buttonRetouch.enabled = true;
    }
    
    @IBAction func buttonPlusTapped(sender: AnyObject) {
        NSLog("plus")
        self.touchState = .Plus
        //self.updateStateLabel()
        touchDrawView.currentState = .Plus
    }
    
    @IBAction func buttonMinusTapped(sender: AnyObject) {
        NSLog("minus")
        self.touchState = .Minus
        //self.updateStateLabel()
        touchDrawView.currentState = .Minus
    }
    
    
    @IBAction func buttonRetouchTapped(sender: AnyObject) {
    
        if((imageView.image != nil)){
            
            
            if touchState == .Rect {
                if self.isUnderMinimumRect() {
                    let alert = UIAlertController(title: "Opps", message: "More bigger rect for operation", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil);
                    return
                }
                self.doGrabcut()
                self.touchDrawView.clear()
                self.buttonRect.enabled = false
                self.buttonPlus.enabled = true
                self.buttonMinus.enabled = true
                self.buttonRetouch.enabled = false
            }
            else {
                if touchState == .Plus || touchState == .Minus {
                    var touchedMask: UIImage = self.touchDrawView.maskImageWithPainting()
                    //self.doGrabcutWithMaskImage(touchedMask)
                    self.touchDrawView.clear()
                    self.buttonRect.enabled = false
                    self.buttonPlus.enabled = true
                    self.buttonMinus.enabled = true
                    self.buttonRetouch.enabled = true
                }
            }
        }
    }
    
            
            
            
            
            
            
            
            
            
            
            
            func doGrabcut() {
            
            let spinner = JHSpinnerView.showOnView(view, spinnerColor:UIColor.redColor(), overlay:.FullScreen, overlayColor:UIColor.blackColor().colorWithAlphaComponent(0.6))

        
            view.addSubview(spinner)
            
            let image:UIImage = imageView.image!;
            //let rect : CGRect = CGRectMake(1.0,1.0,image.size.width - 2.0, image.size.height - 2.0);
            
            self.gcManager.resetManager();
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                var resultImage:UIImage = self.gcManager.doGrabCut(image, foregroundBound: self.grabRect!, iterationCount: 5);
                
                resultImage = self.masking(image, maskImage: resultImage);
                
                dispatch_async(dispatch_get_main_queue()) {
                    
                    self.imageView.image = resultImage;
                    
                    spinner.dismiss();
                }
            }
            
    }
    
    
    @IBAction func saveButtonTapped(sender: AnyObject) {
        UIImageWriteToSavedPhotosAlbum(self.gcManager.saveImageWithWhiteBlackground(imageOriginal), self, nil, nil);
        
        let alert = UIAlertController(title: "Success", message: "Save image to gallery completed", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    
    
    private func masking(sourceImage: UIImage, maskImage: UIImage) -> UIImage{
        //Mask Image
        let maskRef = maskImage.CGImage;
        
        let mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
            CGImageGetHeight(maskRef),
            CGImageGetBitsPerComponent(maskRef),
            CGImageGetBitsPerPixel(maskRef),
            CGImageGetBytesPerRow(maskRef),
            CGImageGetDataProvider(maskRef), nil, false);
        
        let masked = CGImageCreateWithMask(sourceImage.CGImage, mask);
        //CGImageRelease(mask);
        
        let maskedImage = UIImage(CGImage: masked!, scale: 2.0, orientation: UIImageOrientation.Up)
        
        
        return maskedImage;
    }
    
    
    func isUnderMinimumRect() -> Bool {
        if grabRect!.size.width < 20.0 || grabRect!.size.height < 20.0 {
            return true
        }
        return false
    }
    
    
    func getTouchedRectWithImageSize(size: CGSize) -> CGRect {
        var widthScale: CGFloat = size.width / self.imageView.frame.size.width
        var heightScale: CGFloat = size.height / self.imageView.frame.size.height
        return self.getTouchedRect(startPoint, endPoint: endPoint, widthScale: widthScale, heightScale: heightScale)
    }
    
    func getTouchedRect(startPoint: CGPoint, endPoint: CGPoint) -> CGRect {
        return self.getTouchedRect(startPoint, endPoint: endPoint, widthScale: 1.0, heightScale: 1.0)
    }
    
    func getTouchedRect(startPoint: CGPoint, endPoint: CGPoint, widthScale: CGFloat, heightScale: CGFloat) -> CGRect {
        var minX: CGFloat = startPoint.x > endPoint.x ? endPoint.x * widthScale : startPoint.x * widthScale
        var maxX: CGFloat = startPoint.x < endPoint.x ? endPoint.x * widthScale : startPoint.x * widthScale
        var minY: CGFloat = startPoint.y > endPoint.y ? endPoint.y * heightScale : startPoint.y * heightScale
        var maxY: CGFloat = startPoint.y < endPoint.y ? endPoint.y * heightScale : startPoint.y * heightScale
        return CGRectMake(minX, minY, maxX - minX, maxY - minY)
    }
    
    
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?){
        NSLog("began")
        if let touch = touches.first as UITouch! {
            
            self.startPoint = touch.locationInView(self.imageView)
            if touchState == .None || touchState == .Rect {
                self.touchDrawView.clear()
            }
            else {
                if touchState == .Plus || touchState == .Minus {
                    self.touchDrawView.touchStarted(self.startPoint)
                }
            }
        }
        
        //super.touchesBegan(touches , withEvent:event!)
    }

    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        NSLog("moved")
        
        if let touch = touches.first as UITouch! {
        var point: CGPoint = touch.locationInView(self.imageView)
        if touchState == .Rect {
            var rect: CGRect = self.getTouchedRect(startPoint, endPoint: point)
            self.touchDrawView.drawRectangle(rect)
        }
        else {
            if touchState == .Plus || touchState == .Minus {
                self.touchDrawView.touchMoved(point)
            }
        }
        }
        
        //super.touchesMoved(touches , withEvent:event!)
        
    }
    
override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        NSLog("ended")
    if let touch = touches.first as UITouch! {
        self.endPoint = touch.locationInView(self.imageView)
        if touchState == .Rect {
            self.grabRect = self.getTouchedRectWithImageSize(self.imageOriginal!.size)
        }
        else {
            if touchState == .Plus || touchState == .Minus {
                self.touchDrawView.touchEnded(self.endPoint)
                self.buttonRetouch.enabled = true
            }
        }
    }
    //super.touchesEnded(touches , withEvent:event!)
    }
}