//
//  ViewController.h
//  GrabCutIOS
//
//  Created by EunchulJeon on 2015. 8. 29..
//  Copyright (c) 2015 Naver Corp.
//  @Author Eunchul Jeon
//

#import <UIKit/UIKit.h>

@interface GCWrapper : NSObject

- (void)initial:(UIImage*)original resultImageView:(UIImageView*)view foregroundRect:(CGRect) rect;
- (void) doGrabcut;
- (void) resetManager;
@end

